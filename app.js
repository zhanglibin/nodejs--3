var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var login = require('./routes/login');
var zy = require('./routes/zy');
var ip_apply = require('./routes/ip_apply');
var up=require('./routes/update');
var dialig=require('./routes/dialig');
var dele = require('./routes/dele');
var userlog = require('./routes/userlog');
var logout=require('./routes/logout');
var great_ip=require('./routes/great_ip');
var root_ip=require('./routes/root_ip');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/', login);
app.use('/login',login);
app.use('/zy',zy);
app.use('/ip_apply',ip_apply);
app.use('/update',up);
app.use('/dialig',dialig);
app.use('/dele',dele);
app.use('/userlog',userlog);
app.use('/logout',logout);
app.use('/great_ip',great_ip);
app.use('/root_ip',root_ip);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.listen(8000,function(){
    console.log("8000 port on! service up!");
});

module.exports = app;
