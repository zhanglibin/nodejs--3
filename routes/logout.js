/**
 * Created by Administrator on 2015/12/10.
 */
var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
        res.cookie('IP', 'null', {maxAge: 0});
        res.render('logout', {'title': '注销', 'msg': '页面已经注销'});
});

module.exports = router;