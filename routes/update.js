/**
 * Created by Administrator on 2015/12/1.
 */
var express = require('express');
var router = express.Router();
var BaseModel = require('../models/base_model');
var baseModel = new BaseModel();

router.get('/:all', function(req, res) {
    var all=req.params.all;
    arr=all.split('@');
    var id=arr[0];
    var dep=arr[1];
    var ip_name=arr[2];
    var mac=arr[3];
    console.log(all,arr[0],arr[1],arr[2],arr[3]);
    res.render('update', {
        title: id ,
        bumen : dep ,
        name : ip_name ,
        macadress :mac
    });
});

router.post('/', function(req, res) {
    var tableName = 'ips';
    var idJson = {
        'id':req.body.hiddenField
    };
    var rowInfo = {
        'dep':req.body.select,
        'ip_name':req.body.ip_name,
        'mac':req.body.mac,
        'machine':req.body.select2,
        'build':req.body.select3,
        'floor':req.body.select4,
        'remark':req.body.beizhu,
        'auth':req.body.select5
    };
    baseModel.modify(tableName,idJson,rowInfo,function(ret){
        res.redirect('/dialig/0');
    });

});

module.exports = router;
