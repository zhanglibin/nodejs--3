/**
 * Created by Administrator on 2015/11/10 0010.
 */
var express = require('express');
var router = express.Router();
var BaseModel = require('../models/base_model');
var baseModel = new BaseModel();
var CheckLogin = require('../models/checkLogin');
var checkLogin =new CheckLogin();


router.get('/',checkLogin.check);
router.get('/', function(req, res) {
    res.render('ip_apply', {
        title: 'ip申请',
        message:'',
        cook_name:req.cookies.IP
    });
});

router.post('/',checkLogin.check);
router.post('/', function(req, res) {
    var tableName = 'ips';
    var rowInfo = {
        'dep':req.body.select,
        'ip_name':req.body.ip_name,
        'mac':req.body.mac,
        'machine':req.body.select2,
        'build':req.body.select3,
        'floor':req.body.select4,
        'remark':req.body.beizhu,
        'auth':req.body.select5
    };
    baseModel.insert(tableName,rowInfo,function(ret){
        res.render('ip_apply',{
            title:'ip申请',
            message:'ip申请成功',
            cook_name:req.cookies.IP
        });
    });
});

module.exports = router;

