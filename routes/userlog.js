/**
 * Created by Administrator on 2015/12/8 0008.
 */
var express = require('express');
var router = express.Router();
var BaseModel = require('../models/base_model');
var baseModel = new BaseModel();
var CheckLogin = require('../models/checkLogin');
var checkLogin =new CheckLogin();


router.get('/',checkLogin.check);
router.get('/', function(req, res, next) {
    res.render('userlog', {
        title: '登录日志',
        cook_name:req.cookies.IP
    });
});

router.post('/', function(req, res) {
    var uname = req.body.uname;
    var sql = "select user,time from login_log where 1=1" ;
    if(uname.length>0)
    {
        sql=sql+" and user='"+ uname +"'"+"order by time desc";
    }
    baseModel.findMoreById(sql, function (ret){
        res.render('userlog',{
            title: '登录日志',
            info:ret,
            cook_name:req.cookies.IP
        });
    });
});

module.exports = router;
