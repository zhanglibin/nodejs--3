/**
 * Created by Administrator on 2015/11/10 0010.
 */
var express = require('express');
var router = express.Router();
var BaseModel = require('../models/base_model');
var baseModel = new BaseModel();
var CheckLogin = require('../models/checkLogin');
var checkLogin =new CheckLogin();


router.get('/',checkLogin.check);
router.get('/', function(req, res) {
    var sql="select distinct dep from ips order by dep desc";
    baseModel.findMoreById(sql,function(rett){
        global.menu_date=rett;
        res.render('zy', {
            title: '主页',
            bumen: '',
            name: '',
            macadress: '',
            menu:rett ,
            cook_name:req.cookies.IP,
            page:0
        });
        console.log(req.cookies.IP);
    });
});


router.post('/',checkLogin.check);
router.post('/', function(req, res) {
    global.idep=req.body.dep;
    global.iname=req.body.ip_name;
    global.imac=req.body.mac;
    var sql = "select id,dep,ip_name,mac from ips where 1=1";
    if (idep.length > 0) {
        sql = sql + " and dep='" + idep + "'";
    }
    if (iname.length > 0) {
        sql = sql + " and ip_name='" + iname + "'";
    }
    if (imac.length > 0) {
        sql = sql + " and mac='" + imac + "'";
    }
    global.SQL=sql;
    sql=sql+" limit 0,20";
    baseModel.findMoreById(sql,function (ret1) {
            res.render('zy', {
                menu: global.menu_date,
                title: '主页',
                content: ret1,
                bumen: global.idep,
                name: global.iname,
                macadress: global.imac,
                cook_name:req.cookies.IP,
                page: 1
        });
    });
});


router.get('/:p', function(req, res) {
    var page = req.params.p;
    var offset = (page-1)*20;
    var sql=global.SQL+" limit "+offset+",20";

    baseModel.findMoreById(sql ,function (ret) {
        res.render('zy', {
            menu: global.menu_date,
            title: '主页',
            content: ret,
            bumen: global.idep,
            name: global.iname,
            macadress: global.imac,
            cook_name:req.cookies.IP,
            page:parseInt(page)
        });
    });
});


module.exports = router;


